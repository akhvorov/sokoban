#pragma once
#include "Actor.h"
#include "Dot.h"

class Box :
	public Actor
{
public:
	Box();
	Box(Point p);
	void Move(Direction);
	void virtual ReDraw(Direction);
	void ReDraw(Direction, Dot*, int);
	bool virtual CanMove(Matrix, Direction);
	bool operator < (Box);
	bool operator <= (Box);
	bool operator > (Box);
	bool operator >= (Box);
	~Box();
};

