#pragma once
#include <string>
#include <conio.h>
#include <stack>
#include "Matrix.h"
#include "Actor.h"
#include "Box.h"
#include "Player.h"
#include "Dot.h"

using namespace std;

class Scene
{
private:
	Matrix _matrix;
	int _dotCount;
	int _boxCount;
	Dot* _dots;
	Box* _boxes;
	Player _player;
	//��� �� ����� �����
	bool Fail();
	bool NearWall(Point);
	bool InCorner(Point);
	bool OnOneLineWithDot(Point);
	bool AlongWall(Point, int);
	Direction RandomDirection();

public:
	Scene();
	Scene(string);
	void InitializeActors();
	int Play();
	void BotPlay(stack<Direction>, int);
	void ReDraw(Direction);
	bool IsWin();
	Direction GetDirection(char);
	~Scene();
};

