#pragma once
#include <stack>
#include <list>
#include <queue>
#include <hash_set>
#include "Matrix.h"
#include "Actor.h"
#include "Box.h"
#include "Player.h"
#include "Dot.h"
#include "Step.h"
#include <stack>
#define DEPTH 20

class AI
{
private:
	stack<Direction> _stack;
	int _Depth;
	int depth;
	int depthFail;
	string _str;
	string output;
	list<Step> _stepSet;
	list<long long> _hashSet;
	hash_set<long long> set;
	hash_set<long long>::const_iterator iterator;

public:
	list<Direction> _dirs;
	Direction directions[4];

	AI();
	AI(string, string);
	AI(string, string, int);
	void InitializeActors();
	void Find(int n = 0, int l = 0, int e = 10);
	void Play();
	bool FindPath(Step &st, Direction);
	bool AddToSet(long long);
	void Write();
	Direction GetDirection();
	stack<Direction> GetPath();
	void Show(Step, Direction);
	void Print();
	~AI();
};

