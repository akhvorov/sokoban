#include "stdafx.h"
#include "Game.h"


Game::Game()
{
	InvisibleCursor();
	GetMaxLevel();
	GetLevel();
}

void Game::Play(){
	char key;
	_level = _level % (MaxLevel + 1);
	if (_level == 0)
		_level++;
	if (_level > 1 && _level < MaxLevel){ //������ ��� ����������
		cout << "Enter - continue game, Space - start new" << endl;
		key = _getch();
		while ((int)key != 32 && (int)key != 13)
			key = _getch();
		if ((int)key == 32){
			_level = 1;
			SaveLevel();
		}
	}
	int n = 1;
	while (n == 1)
		n = PlayLevel();
	if (n == 0){
		system("cls");
		Point(0, 10).SetCursor();
		cout << "GoodBye!" << endl;
		return;
	}
	if (n == 2){
		system("cls");
		cout << "Congratulations! You are winner!" << endl;
	}
}
//0 - �������, 1 - ����������, 2 - ������!
int Game::PlayLevel(){
	MakeFileName();
	system("cls");
	cout << "You can control player by arrows. For exit press ESC. For restart Enter." << endl;
	cout << "For start press Enter" << endl;
	char key = _getch();
	while ((int)key != 13 && (int)key != 27)
		key = _getch();
	int b = 1;
	if (key == 13){ //������ Enter
		system("cls");
		b = Scene(_fileName).Play();
		while (b == 0){
			system("cls");
			b = Scene(_fileName).Play();
		}
	}
	if (key == 27 || b == 2)//������ esc
		return 0;
	if (b == 1){ //������ �������
		_level++;
		SaveLevel();
		if (_level > MaxLevel)
			return 2; //win
		else
			return 1; //continue
	}
}

void Game::InvisibleCursor(){
	HANDLE hCons = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cursor = { 1, false };
	SetConsoleCursorInfo(hCons, &cursor);
}

void Game::GetLevel(){
	ifstream in("..\level.txt");
	string s;
	in >> s;
	in.close();
	_fileName = "Maps\\..\level" + s + ".txt";
	_level = StringToInt(s);
}

void Game::SaveLevel(){
	ofstream out("..\level.txt");
	out << _level;
	out.close();
}

void Game::GetMaxLevel(){
	ifstream in("..\MaxLevel.txt");
	string s;
	in >> s;
	in.close();
	MaxLevel = StringToInt(s);
}

int Game::StringToInt(string s){
	int n = 0;
	int r = 1;
	for (int i = s.length() - 1; i >= 0; i--){
		n += r*(s[i] - '0');
		r *= 10;
	}
	return n;
}

string Game::IntToString(int n){
	string s = "";
	while (n > 0){
		s = (char)(n + '0') + s;
		n = n / 10;
	}
	return s;
}

void Game::MakeFileName(){
	_fileName = "Maps\\..\level" + IntToString(_level) + ".txt";
}

Game::~Game()
{
}
