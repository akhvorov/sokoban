#include "stdafx.h"
#include "Step.h"


Step::Step()
{
}

Step::Step(string s){
	_matrix = Matrix(s);
	InitializeActors();
}

Step::Step(Matrix m, Dot* d, Box* b, Player p, int dC, int bC){
	_matrix = m;
	_dots = d;
	_dotCount = dC;
	_boxes = b;
	_boxCount = bC;
	_player = p;
}

void Step::InitializeActors(){
	_dotCount = 0;
	_boxCount = 0;
	int pl = 0;
	for (int h = 0; h < _matrix.height; h++){
		for (int w = 0; w < _matrix.width; w++){
			switch ((TypeOfCell)(_matrix.mas[h][w])){
			case DotType:
				_dotCount++;
				break;
			case BoxType:
				_boxCount++;
				break;
			case PlayerType:
				pl++;
				break;
			case PlayerOnDot:
				pl++;
				_dotCount++;
				break;
			case BoxOnDot:
				_boxCount++;
				_dotCount++;
				break;
			}
		}
	}
	if (pl != 1){
		cout << "wrong file!!!" << endl;
	}
	_dots = new Dot[_dotCount];
	_boxes = new Box[_boxCount];
	int d = 0, b = 0;
	for (int h = 0; h < _matrix.height; h++){
		for (int w = 0; w < _matrix.width; w++){
			Point p(w, h);
			switch ((Direction)(_matrix.mas[h][w])){
			case DotType:
				_dots[d] = Dot(p);
				d++;
				break;
			case BoxType:
				_boxes[b] = Box(p);
				b++;
				break;
			case PlayerType:
				_player = Player(p);
				break;
			case PlayerOnDot:
				_player = Player(p);
				_dots[d] = Dot(p);
				d++;
				break;
			case BoxOnDot:
				_boxes[b] = Box(p);
				_dots[d] = Dot(p);
				b++;
				d++;
				break;
			}
		}
	}
}

//0 - �������, 1 - ������, 2 - �����
int Step::Play(){
	char key;
	_matrix.Print();
	key = _getch();
	while ((int)key != 27){
		if (_player.CanMove(_matrix, GetDirection(key), _boxes, _boxCount) && GetDirection(key) != None){
			ReDraw(GetDirection(key));
		}
		if (IsWin()){
			return 1;
		}
		if ((int)key == 13){ //������ �������
			return 0;
		}
		if ((int)key == 27){ //������ �����
			return 2;
		}
		key = _getch();
	}
	if (IsWin()){
		return 1;
	}
	return 2;
}

void Step::ReDraw(Direction dir){
	_boxes = _player.ReDraw(dir, _boxes, _boxCount, _dots, _dotCount); //��������� �������, ������ ��� �������� �� ����������
}

bool Step::Move(Direction dir){
	bool b = false;
	Point* m = new Point[_boxCount];
	for (int i = 0; i < _boxCount; i++){
		m[i] = Point(_boxes[i].GetPoint().x, _boxes[i].GetPoint().y);
	}
	_boxes = _player.Move(dir, _boxes, _boxCount);
	for (int i = 0; i < _boxCount; i++){
		if (_boxes[i].GetPoint() != m[i]){
			b = true;
		}
	}
	return b;
}

void Step::MoveBack(Direction dir, bool b){
	_boxes = _player.MoveBack(dir, _boxes, _boxCount, b);
}

bool Step::CanMove(Direction dir){
	return _player.CanMove(_matrix, dir, _boxes, _boxCount);
}

Direction Step::GetDirection(char c){
	switch ((int)c)
	{
	case 72: return Up;
	case 75:return Left;
	case 77: return Right;
	case 80: return Down;
	default:
		return None;
	}
}

bool Step::EmptyMove(){
	for (int i = 0; i < _boxCount; i++){
		if (_player.empty < _matrix.empty){
			return false;
		}
	}
	return true;
}

long long Step::GetHash(){
	for (int i = _boxCount - 1; i >= 0; i--){
		for (int j = 0; j < i; j++){
			if (_boxes[j] > _boxes[j + 1]){
				swap(_boxes[j], _boxes[j + 1]);
			}
		}
	}
	long long n = 0;
	int r = 1;
	Point p;
	for (int i = 0; i < _boxCount; i++){
		p = _boxes[i].GetPoint();
		n *= 10;
		if (p.x > 9)
			n *= 10;
		n += p.x;
		n *= 10;
		if (p.y > 9)
			n *= 10;
		n += p.y;
	}
	p = _player.GetPoint();
	n *= 10;
	if (p.x > 9)
		n *= 10;
	n += p.x;
	n *= 10;
	if (p.y > 9)
		n *= 10;
	n += p.y;
	return n;
}

string Step::IntToStr(int n){
	string s = "";
	while (n > 0){
		s = (char)(n % 10 - '0') + s;
		n /= 10;
	}
	return s;
}

bool Step::IsWin(){
	bool t = false;
	for (int i = 0; i < _dotCount; i++){
		for (int j = 0; j < _boxCount; j++){ //��������� ��������� �� ��� ����� ���� ���� � ����� ��������
			if (_dots[i].IsMatch(_boxes[j]))
				t = true;
		}
		if (!t){ //���� ���� ���� ����� �� ������, �� �� �������
			return false;
		}
		t = false;
	}
	return true;
}

bool Step::IsFail(){
	bool b = false;
	bool v = false; //����� �� ������� �� �����-�� �����
	for (int i = 0; i < _boxCount; i++){
		for (int j = 0; j < _dotCount; j++){
			if (_boxes[i].GetPoint() == _dots[j].GetPoint()){ //������ � ���� ������ ��� ������
				v = true;
				break;
			}
		}
		if (!v){ //���� �� ��� ���������
			if (NearWall(_boxes[i].GetPoint())){
				return true;
			}
		}
	}
	return false;
}

bool Step::NearWall(Point p){
	int a, b, c = 0;
	for (int i = 0; i < 4; i++){
		if (!_matrix.IsEmpty(p + Point((Direction)i))){
			if (InCorner(p))
				return true;
			if (OnOneLineWithDot(p))
				return false;
			if (AlongWall(p, i))
				return true;
		}
	}
	return false;
}

bool Step::InCorner(Point p){
	int t = 0;
	for (int i = 0; i < 4; i++)
		if (!_matrix.IsEmpty(p + Point((Direction)i)))
			return (!_matrix.IsEmpty(p + Point((Direction)((i - 1) % 4))) || !_matrix.IsEmpty(p + Point((Direction)((i + 1) % 4))));
	return false;
}

bool Step::OnOneLineWithDot(Point p){
	bool b = false; //���� �� �� ���� ���-��
	for (int j = 0; j < _dotCount; j++){
		if (p.x == _dots[j].GetPoint().x){
			for (int i = min(p.y, _dots[j].GetPoint().y); i < max(p.y, _dots[j].GetPoint().y); i++)
				if (!_matrix.IsEmpty(Point(p.x, i)))
					b = true;
			if (!b)
				return true;
			b = false;
		}
		if (p.y == _dots[j].GetPoint().y){
			for (int i = min(p.x, _dots[j].GetPoint().x); i < max(p.x, _dots[j].GetPoint().x); i++)
				if (!_matrix.IsEmpty(Point(i, p.y)))
					b = true;
			if (!b)
				return true;
			b = false;
		}
	}
	return false;
}

bool Step::AlongWall(Point p, int i){
	int a, b, c = 0;
	bool h = false;
	if (i % 2 == 0){ //����/���
		a = p.x;
		b = p.x;
		while (_matrix.IsEmpty(Point(a, p.y)))
			a--;
		a++;
		while (_matrix.IsEmpty(Point(b, p.y)))
			b++;
		if (b - a < 2)
			h = true;
		for (int j = a; j < b; j++)
			if (_matrix.IsEmpty(Point(j, (Point((Direction)i) + p).y))){
				c++;
				if (j + 1 < b && _matrix.IsEmpty(Point(j + 1, (Point((Direction)i) + p).y)))
					h = true;
			}
	}
	else{ //�����/����
		a = p.y;
		b = p.y;
		while (_matrix.IsEmpty(Point(p.x, a)))
			a--;
		a++;
		while (_matrix.IsEmpty(Point(p.x, b)))
			b++;
		if (b - a < 2)
			h = true;
		for (int j = a; j < b; j++)
			if (_matrix.IsEmpty(Point((Point((Direction)i) + p).x, j))){
				c++;
				if (j + 1 < b && _matrix.IsEmpty(Point((Point((Direction)i) + p).x, j + 1)))
					h = true;
			}
	}
	return c == 0;
}

bool Step::operator ==(Step st){
	if (_player.GetPoint() != st._player.GetPoint())
		return false;
	for (int i = 0; i < _boxCount; i++)
		if (_boxes[i].GetPoint() != st._boxes[i].GetPoint())
			return false;
	return true;
}

void Step::Print(){
	system("cls");
	_matrix.PrintLab();
	_boxes = _player.ReDraw(None, _boxes, _boxCount, _dots, _dotCount);
}

void Step::Show(int n){
	_matrix.PrintLab();
}

Step::~Step()
{
}
