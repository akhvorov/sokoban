#pragma once
#include <windows.h>

enum Direction{
	Up, Right, Down, Left, None
};

template<typename T> void Sort(T* m, int n){
	for (int i = n - 1; i >= 0; i--)
		for (int j = 0; j < i; j++)
			if (m[j] > m[j + 1])
				swap(m[j], m[j + 1]);
}

class Point
{
public:
	int x;
	int y;
	Point();
	~Point();
	Point(Direction);
	Point(int, int);
	void SetCursor();
	bool operator == (Point);
	bool operator != (Point);
	Point operator + (Point);
	Point operator - (Point);
	bool operator < (Point);
	bool operator <= (Point);
	bool operator > (Point);
	bool operator >= (Point);
};



