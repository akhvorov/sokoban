﻿#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "Point.h"

using namespace std;

class Matrix
{
private:
	string FileName;
	char symbols[5];
public:
	int width;
	int height;
	int** mas;
	int empty;

	Matrix();
	Matrix(string s);
	void TakeSize();
	void InitializeMassive();
	void InputMassive();
	void Print();
	void PrintLab();
	bool IsEmpty(Point p);
	~Matrix();
};

enum TypeOfCell{
	Empty = 0,
	Wall = 1,
	DotType = 2, 
	BoxType = 3,
	PlayerType = 4,
	BoxOnDot = 5,
	PlayerOnDot = 6
};

