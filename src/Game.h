#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
#include "Scene.h"

class Game
{
private:
	int _level;
	int MaxLevel;
	string _fileName;

public:
	Game();
	void Play();
	int PlayLevel();
	void InvisibleCursor();
	void GetLevel();
	void SaveLevel();
	void GetMaxLevel();
	int StringToInt(string);
	string IntToString(int);
	void MakeFileName();
	~Game();
};

