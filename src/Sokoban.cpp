// Sokoban.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
#include <time.h>
#include "Scene.h"
#include "Game.h"
#include "AI.h"

using namespace std;

int N = 500;
string fileName = "..\test.txt";
string decision = "..\Desicion.txt";


void InvisibleCursor(){
	HANDLE hCons = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cursor = { 1, false };
	SetConsoleCursorInfo(hCons, &cursor);
}

void Play(){
	InvisibleCursor();
	cout << "You can control player by arrows. For exit press ESC. For restart Enter." << endl;
	cout << "For start press Enter" << endl;
	char key = _getch();
	while ((int)key != 13 && (int)key != 27)
		key = _getch();
	bool b = false;
	if (key == 13){
		system("cls");
		Scene sc(fileName);
		b = sc.Play();
		while (b){
			system("cls");
			b = Scene(fileName).Play();
		}
	}
}

//������ � ���� ������������������ �����
void Write(stack<Direction> dirs){
	ofstream out(decision);
	Direction dir;
	cout << dirs.size() << endl;
	while(!dirs.empty()){
		dir = dirs.top();
		dirs.pop();
		switch (dir)
		{
		case Up:
			out << "Up";
			break;
		case Right:
			out << "Right";
			break;
		case Down:
			out << "Down";
			break;
		case Left:
			out << "Left";
			break;
		case None:
			out << "None";
			break;
		default:
			break;
		}
		out << endl;
	}
	out.close();
}

void Translate(){
	ifstream in(fileName);
	string s, text = "";
	while (getline(in, s))
		text += s + '\n';
	in.close();
	for (int i = 0; i < text.length(); i++)
		switch (text[i]){
		case ' ': text[i] = '0'; break;
		case '#': text[i] = '1'; break;
		case '.': text[i] = '2'; break;
		case '$': text[i] = '3'; break;
		case '@': text[i] = '4'; break;
		case '*': text[i] = '5'; break;
		case '+': text[i] = '6'; break;
		}
	ofstream out(fileName);
	out << text;
	out.close();
}

void BotPlay(){
	InvisibleCursor();
	int n;
	cout << "Input number of steps" << endl;
	cin >> n;
	AI ai(fileName, decision, n);
	clock_t c1 = clock();
	ai.Find(n, 0, 2);
	while (ai.GetPath().size() == 0){
		n += 50;
		ai.Find(n, n - 50, 2);
	}
	clock_t c2 = clock();
	cout << "Time: " << (double)(c2 - c1) / (double)CLOCKS_PER_SEC << endl;
	stack<Direction> dirs = ai.GetPath();
	Write(dirs);
	cout << "Press Enter to continue" << endl;
	Sleep(4000);
	char c;
	c = _getch();
	while (c != 13)
		c = _getch();
	bool b = false;
	Scene sc(fileName);
	sc.BotPlay(dirs, 250);
}

void PlayFromFile(){
	ifstream in(decision);
	string s;
	stack<Direction> d;
	stack<Direction> dirs;
	while (getline(in, s)){
		if (s == "0" || s == "Up")
			d.push(Up);
		else if (s == "1" || s == "Right")
			d.push(Right);
		else if (s == "2" || s == "Down")
			d.push(Down);
		else if (s == "3" || s == "Left")
			d.push(Left);
	}
	Direction dir;
	while (!d.empty()){
		dir = d.top();
		d.pop();
		dirs.push(dir);
	}
	Scene sc(fileName);
	sc.BotPlay(dirs, 200);
}

int _tmain(int argc, _TCHAR* argv[])
{
	//Game().Play();

	Translate(); //��������� � ������, �������� ���, �������
	BotPlay(); //���� ������� � �������
	//PlayFromFile(); //��������� ������� �� �����, ���� ��������� �������

	//Play();
	//FindWay();
	system("pause");
	return 0;
}

