﻿#include "stdafx.h"
#include "Player.h"


Player::Player() : Actor()
{
}

Player::Player(Point p) : Actor(p, (char)1){

}
void Player::ReDraw(Direction dir){

}

Box* Player::ReDraw(Direction dir, Box* boxes, int len, Dot* dots, int dotLen){
	Erase();
	for (int i = 0; i < dotLen; i++)
		if (dots[i].GetPoint() == _point)
			dots[i].Draw();
	_point = _point + Point(dir);
	empty++;
	int t = 10;
	for (int i = 0; i < len; i++){
		Point(0, t).SetCursor();
		t++;
		if (boxes[i].GetPoint() == _point){
			boxes[i].ReDraw(dir);
			empty = 0;
		}
	}
	Draw();
	return boxes;
}

Box* Player::Move(Direction dir, Box* boxes, int len){
	_point = _point + Point(dir);
	empty++;
	for (int i = 0; i < len; i++){
		if (boxes[i].GetPoint() == _point){
			boxes[i].Move(dir);
			empty = 0;
		}
	}
	return boxes;
}

Box* Player::MoveBack(Direction d, Box* boxes, int len, bool b){
	Direction dir = (Direction)((((int)d) + 2) % 4); //обратное направление
	empty--;
	for (int i = 0; i < len; i++)
		if (boxes[i].GetPoint() == _point + Point(d) && b)
			boxes[i].Move(dir);
	_point = _point + Point(dir);
	return boxes;
}

bool Player::CanMove(Matrix m, Direction d){
	return m.IsEmpty(_point + Point(d));
}

bool Player::CanMove(Matrix m, Direction d, Box* boxes, int len){
	if (!m.IsEmpty(_point + Point(d)))
		return false;
	bool b = true;
	for (int i = 0; i < len; i++){
		if (boxes[i].GetPoint() == (_point + Point(d))){ //если в том направлении есть коробка, нужно проверять, нет ли еще одной
			if (!m.IsEmpty(boxes[i].GetPoint() + Point(d)))
				return false;
			for (int j = 0; j < len; j++)
				if ((i != j) && (boxes[j].GetPoint() == (boxes[i].GetPoint() + Point(d)))) //если после коробки коробка
					return false;
		}
	}
	return true;
}

Player::~Player()
{
}
