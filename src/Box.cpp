#include "stdafx.h"
#include "Box.h"


Box::Box() : Actor()
{
}

Box::Box(Point p) : Actor(p, (char)(253)){

}

void Box::Move(Direction dir){
	_point = _point + Point(dir);
}

void Box::ReDraw(Direction dir){
	Erase();
	_point = _point + Point(dir);
	Draw();
}
void Box::ReDraw(Direction dir, Dot* dots, int len){
	Erase();
	_point = _point + Point(dir);
	for (int i = 0; i < len; i++)
		if (dots[i].GetPoint() == _point)
			system("color 3");
	Draw();
	system("color 0");
}

bool Box::CanMove(Matrix mas, Direction d){
	return false;
}

bool Box::operator < (Box b){
	return _point < b.GetPoint();
}
bool Box::operator <= (Box b){
	return _point <= b.GetPoint();
}
bool Box::operator > (Box b){
	return _point > b.GetPoint();
}
bool Box::operator >= (Box b){
	return _point >= b.GetPoint();
}
Box::~Box()
{
}
