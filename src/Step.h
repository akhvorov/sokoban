#pragma once
#include <string>
#include <conio.h>
#include "Matrix.h"
#include "Actor.h"
#include "Box.h"
#include "Player.h"
#include "Dot.h"

class Step
{
private:
	bool NearWall(Point);
	bool InCorner(Point);
	bool OnOneLineWithDot(Point);
	bool AlongWall(Point, int);
	Direction RandomDirection();

public:
	Matrix _matrix;
	int _dotCount;
	int _boxCount;
	Dot* _dots;
	Box* _boxes;
	Player _player;

//public:
	Step();
	Step(string);
	Step(Matrix, Dot*, Box*, Player, int, int);
	void InitializeActors();
	int Play();
	void ReDraw(Direction);
	bool Move(Direction);
	void MoveBack(Direction, bool);
	bool CanMove(Direction);
	bool IsWin();
	bool IsFail();
	long long GetHash();
	bool EmptyMove();
	Direction GetDirection(char);
	bool operator ==(Step);
	void Print();
	void Show(int n = 0);
	string IntToStr(int);
	void Sort();
	~Step();
};

