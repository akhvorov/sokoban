#include "stdafx.h"
#include "Scene.h"


Scene::Scene()
{
}

Scene::Scene(string s){
	system("cls");
	_matrix = Matrix(s);
	InitializeActors();
}

void Scene::InitializeActors(){
	_dotCount = 0;
	_boxCount = 0;
	int pl = 0;
	for (int h = 0; h < _matrix.height; h++)
		for (int w = 0; w < _matrix.width; w++)
			switch ((Direction)(_matrix.mas[h][w])){
			case DotType:
				_dotCount++;
				break;
			case BoxType:
				_boxCount++;
				break;
			case PlayerType:
				pl++;
				break;
			case PlayerOnDot:
				pl++;
				_dotCount++;
				break;
			case BoxOnDot:
				_boxCount++;
				_dotCount++;
				break;
			}
	if (pl != 1)
		cout << "wrong file!!!" << endl;
	_dots = new Dot[_dotCount];
	_boxes = new Box[_boxCount];
	int d = 0, b = 0;
	for (int h = 0; h < _matrix.height; h++){
		for (int w = 0; w < _matrix.width; w++){
			Point p(w, h);
			switch ((Direction)(_matrix.mas[h][w])){
			case DotType:
				_dots[d] = Dot(p);
				d++;
				break;
			case BoxType: 
				_boxes[b] = Box(p);
				b++;
				break;
			case PlayerType:
				_player = Player(p);
				break;
			case PlayerOnDot:
				_player = Player(p);
				_dots[d] = Dot(p);
				d++;
				break;
			case BoxOnDot:
				_boxes[b] = Box(p);
				_dots[d] = Dot(p);
				b++;
				d++;
				break;
			}
		}
	}
}

//0 - �������, 1 - ������, 2 - �����
int Scene::Play(){
	char key;
	_matrix.Print();
	key = _getch();
	while ((int)key != 27){
		if (_player.CanMove(_matrix, GetDirection(key), _boxes, _boxCount) && GetDirection(key) != None)
			ReDraw(GetDirection(key));
		if (IsWin())
			return 1;
		if ((int)key == 13) //������ �������
			return 0;
		if ((int)key == 27) //������ �����
			return 2;
		if (Fail())
			return 0;
		key = _getch();
	}
	if (IsWin())
		return 1;
	return 2;
}

void Scene::BotPlay(stack<Direction> dirs, int delay){
	_matrix.Print();
	Direction dir;
	while (!dirs.empty()){
		Sleep(delay);
		dir = dirs.top();
		dirs.pop();
		if (_player.CanMove(_matrix, dir, _boxes, _boxCount) && dir != None)
			ReDraw(dir);
		if (IsWin())
			break;
	}
}

void Scene::ReDraw(Direction dir){
	_boxes = _player.ReDraw(dir, _boxes, _boxCount, _dots, _dotCount); //��� ����, ������ ��� �������� ���������� �������
}

Direction Scene::GetDirection(char c){
	switch ((int)c)
	{
	case 72: return Up;
	case 75 :return Left;
	case 77: return Right; 
	case 80: return Down;
	default:
		return None;
	}
}

Direction Scene::RandomDirection(){
	int r = rand() % 4; //��� �������
	return (Direction)r;
}

bool Scene::IsWin(){
	bool t = false;
	for (int i = 0; i < _dotCount; i++){
		for (int j = 0; j < _boxCount; j++)
			if (_dots[i].IsMatch(_boxes[j]))
				t = true;
		if (!t)
			return false;
		t = false;
	}
	return true;
}

bool Scene::Fail(){
	bool b = false;
	bool v = false; //����� �� ������� �� �����-�� �����
	for (int i = 0; i < _boxCount; i++){
		for (int j = 0; j < _dotCount; j++)
			if (_boxes[i].GetPoint() == _dots[j].GetPoint()){ //������ � ���� ������ ��� ������
				v = true;
				break;
			}
		if (!v) //���� �� ��� ���������
			if (NearWall(_boxes[i].GetPoint()))
				return true;
	}
	return false;
}

bool Scene::NearWall(Point p){
	int a, b, c = 0;
	for (int i = 0; i < 4; i++){
		if (!_matrix.IsEmpty(p + Point((Direction)i))){
			if (InCorner(p))
				return true;
			if (OnOneLineWithDot(p))
				return false;
			if (AlongWall(p, i))
				return true;
		}
	}
	return false;
}

bool Scene::InCorner(Point p){
	for (int i = 0; i < 4; i++)
		if (!_matrix.IsEmpty(p + Point((Direction)i)))
			return (!_matrix.IsEmpty(p + Point((Direction)((i - 1) % 4))) || !_matrix.IsEmpty(p + Point((Direction)((i + 1) % 4))));
	return false;
}

bool Scene::OnOneLineWithDot(Point p){
	bool b = false; //���� �� �� ���� ���-��
	for (int j = 0; j < _dotCount; j++){
		if (p.x == _dots[j].GetPoint().x){
			for (int i = min(p.y, _dots[j].GetPoint().y); i < max(p.y, _dots[j].GetPoint().y); i++)
				if (!_matrix.IsEmpty(Point(p.x, i)))
					b = true;
			if (!b)
				return true;
			b = false;
		}
		if (p.y == _dots[j].GetPoint().y){
			for (int i = min(p.x, _dots[j].GetPoint().x); i < max(p.x, _dots[j].GetPoint().x); i++)
				if (!_matrix.IsEmpty(Point(i, p.y)))
					b = true;
			if (!b)
				return true;
			b = false;
		}
	}
	return false;
}

bool Scene::AlongWall(Point p, int i){
	int a, b, c = 0;
	bool h = false;
	if (i % 2 == 0){ //����/���
		a = p.x;
		b = p.x;
		while (_matrix.IsEmpty(Point(a, p.y)))
			a--;
		a++;
		while (_matrix.IsEmpty(Point(b, p.y)))
			b++;
		if (b - a < 2)
			h = true;
		Point(0, 10).SetCursor();
		cout << b - a << endl;
		for (int j = a; j < b; j++)
			if (_matrix.IsEmpty(Point(j, (Point((Direction)i) + p).y))){
				c++;
				if (j + 1 < b && _matrix.IsEmpty(Point(j + 1, (Point((Direction)i) + p).y)))
					h = true;
			}
	}
	else{ //�����/����
		a = p.y;
		b = p.y;
		while (_matrix.IsEmpty(Point(p.x, a)))
			a--;
		a++;
		while (_matrix.IsEmpty(Point(p.x, b)))
			b++;
		if (b - a < 2)
			h = true;
		Point(0, 10).SetCursor();
		cout << b - a << endl;
		for (int j = a; j < b; j++)
			if (_matrix.IsEmpty(Point((Point((Direction)i) + p).x, j))){
				c++;
				if (j + 1 < b && _matrix.IsEmpty(Point((Point((Direction)i) + p).x, j + 1)))
					h = true;
			}
	}
	return !h;
}

Scene::~Scene()
{
}
