#pragma once
#include "Actor.h"
#include "Box.h"
#include "Dot.h"

class Player :
	public Actor
{
public:
	int empty;
	Player();
	Player(Point p);
	virtual void ReDraw(Direction);
	Box* ReDraw(Direction, Box*, int, Dot*, int);
	Box* Move(Direction, Box*, int);
	Box* MoveBack(Direction, Box*, int, bool);
	bool virtual CanMove(Matrix, Direction);
	bool virtual CanMove(Matrix, Direction, Box*, int);
	~Player();
};

