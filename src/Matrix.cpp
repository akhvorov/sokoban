﻿#include "stdafx.h"
#include "Matrix.h"
#include <string>
#include <ctime>


Matrix::Matrix()
{
}

Matrix::Matrix(string FName){
	symbols[0] = ' ';
	symbols[1] = (char)(-37);
	symbols[2] = (char)(-6);
	symbols[3] = (char)(253);
	symbols[4] = (char)1;
	FileName = FName;
	empty = 0;
	TakeSize();
	InitializeMassive();
	InputMassive();
}

void Matrix::TakeSize(){
	ifstream in;
	string s;
	in.open(FileName);
	width = 0;
	height = 0;
	while (getline(in, s)){
		if (width < s.length())
			width = s.length();
		height++;
	}
	in.close();
}

void Matrix::InitializeMassive(){
	mas = new int*[height];
	srand(time(0));
	for (int i = 0; i < height; i++)
		mas[i] = new int[width];
}

void Matrix::InputMassive(){
	ifstream in;
	string s;
	in.open(FileName);
	int pl = 0;
	while (getline(in, s)){
		for (int j = 0; j < s.length(); j++){
			if (s[j] == '0')
				empty++;
			mas[pl][j] = (int)(s[j] - '0');
		}
		for (int i = s.length(); i < width; i++)
			mas[pl][i] = 1;
		pl++;
	}
	in.close();
}

void Matrix::Print(){
	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			if (mas[i][j] < 5)
				cout << symbols[mas[i][j]];
			else{
				if (mas[i][j] == 5)
					cout << symbols[3];
				else
					cout << symbols[4];
			}
		}
		cout << endl;
	}
}

void Matrix::PrintLab(){
	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			if (mas[i][j] == Wall)
				cout << symbols[mas[i][j]];
			else
				cout << " ";
		}
		cout << endl;
	}
}

bool Matrix::IsEmpty(Point p){
	if (p.x < 0 || p.x > width || p.y < 0 || p.y > height)
		return false;
	return (mas[p.y][p.x] != (int)Wall);
}

Matrix::~Matrix()
{
}
