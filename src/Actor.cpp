#include "stdafx.h"
#include "Actor.h"


Actor::Actor()
{

}

Actor::Actor(Point p, char c){
	_point = p;
	_face = c;
	_dir = None;
}

void Actor::Erase(){
	_point.SetCursor();
	cout << ' ';
}

void Actor::Draw(){
	_point.SetCursor();
	cout << _face;
}

void Actor::ReDraw(Direction){

}

bool Actor::CanMove(Matrix, Direction){
	return false;
}

bool Actor::operator == (Actor a){
	return this->_point == a._point;
}

Point Actor::GetPoint(){
	return _point;
}

void Actor::gotoXY(Point p){
	COORD coord;
	coord.X = p.x;
	coord.Y = p.y;
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE),
		coord
		);
}

void Actor::gotoXY(int x, int y){
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE),
		coord
	);
}

Actor::~Actor()
{
}
