#include "stdafx.h"
#include "AI.h"


AI::AI()
{
}

AI::AI(string s, string out)
{
	_str = s;
	output = out;
	depth = 0;
	_Depth = 100;
	depthFail = false;
	directions[0] = Up;
	directions[1] = Right;
	directions[2] = Down;
	directions[3] = Left;
}

AI::AI(string s, string out, int n)
{
	_str = s;
	output = out;
	depth = 0;
	_Depth = n - 2;
	directions[0] = Up;
	directions[1] = Right;
	directions[2] = Down;
	directions[3] = Left;
}

void AI::Find(int MaxDepth, int leftSide, int e){
	if (MaxDepth == 0)
		MaxDepth = 100;
	int k = 0;
	int l, r;
	l = leftSide;
	r = MaxDepth;
	_Depth = (l + r) / 2;
	int y = 1;
	while (r - l >= e){
		cout << y << ". Depth: " << _Depth << endl;
		y++;
		Play();
		cout << endl;
		//���� �� ���������, ���� � ���� ���������� ���� �������
		if (_stack.size() == 0)
			l = _Depth;
		else{
			cout << "It's okay!" << endl;
			Write();
			r = _Depth;
			while (!_stack.empty())
				_stack.pop();
		}
		set.clear();
		_Depth = (l + r) / 2;
	}
	_Depth = r;
	Play();
}

void AI::Play(){
	bool b = false;
	for (int i = 0; i < 4; i++){
		Step st(_str);
		if (st.CanMove(directions[i]) && !b){
			b = FindPath(st, directions[i]);
			if (b) _stack.push(directions[i]);
		}
	}
	cout << "Set Size: " << set.size() << endl;
}

bool AI::FindPath(Step &st, Direction dir){
	bool v = st.Move(dir);
	if (st.IsWin()){
		st.MoveBack(dir, v);
		return true;
	}
	if (!AddToSet(st.GetHash())){
		st.MoveBack(dir, v);
		return false;
	}
	if (st.IsFail() || st.EmptyMove()){
		st.MoveBack(dir, v);
		return false;
	}
	//����������� �� �������
	if (depth > _Depth){
		depthFail++;
		set.erase(st.GetHash());
		st.MoveBack(dir, v);
		return false;
	}
	bool b = false;
	depth++;
	for (int i = 0; i < 4; i++){
		if (st.CanMove(directions[i]) && !b){
			b = FindPath(st, directions[i]);
			if (b) _stack.push(directions[i]);
		}
	}
	if (depthFail == 4)
		set.erase(st.GetHash());
	depth--;
	st.MoveBack(dir, v);
	return b;
}

bool AI::AddToSet(long long n){
	iterator = set.find(n);
	if (iterator == set.end()){
		set.insert(n);
		if (set.size() % 50000 == 0)
			cout << set.size() << endl;
		return true;
	}
	else
		return false;
}

void AI::Write(){
	stack<Direction> dirs = _stack;
	ofstream out(output);
	Direction dir;
	cout << dirs.size() << endl;
	while (!dirs.empty()){
		dir = dirs.top();
		dirs.pop();
		out << dir << endl;
	}
}

Direction AI::GetDirection(){
	return None;
}

stack<Direction> AI::GetPath(){
	return _stack;
}

void AI::Show(Step st, Direction dir){
	system("cls");
	for (int i = 0; i < depth; i++)
		cout << " ";
	cout << depth << ": ";
	switch (dir)
	{
	case Up:
		cout << "Up";
		break;
	case Right:
		cout << "Right";
		break;
	case Down:
		cout << "Down";
		break;
	case Left:
		cout << "Left";
		break;
	case None:
		cout << "None";
		break;
	default:
		break;
	}
	cout << " (" << st._player.GetPoint().x << ", " << st._player.GetPoint().y << ")" << endl;
}

void AI::Print(){
	char c;
	Direction d;
	for (int i = 0; i < _dirs.size(); i++){
		d = _dirs.front();
		switch (d)
		{
		case Up: c = 'u'; break;
		case Right: c = 'r'; break;
		case Down: c = 'd'; break;
		case Left: c = 'l'; break;
		default:
			break;
		}
		cout << c << " ";
	}
	cout << endl;
}

AI::~AI()
{
}
