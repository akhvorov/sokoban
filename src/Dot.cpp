#include "stdafx.h"
#include "Dot.h"


Dot::Dot()
{
}

Dot::Dot(Point p)
{
	_point = p;
	_face = (char)(-6);
}

void Dot::Draw(){
	_point.SetCursor();
	cout << _face;
}

bool Dot::IsMatch(Point p){
	return _point == p;
}

bool Dot::IsMatch(Actor a){
	return _point == a.GetPoint();
}

void Dot::ReDraw(){

}

Point Dot::GetPoint(){
	return _point;
}

Dot::~Dot()
{
}
