#pragma once
#include <windows.h>
#include "Point.h"
#include "Matrix.h"

class Actor
{
protected:
	char _face;
	Point _point;
	Direction _dir;
	
public:
	Actor();
	Actor(Point p, char c);
	void Erase();
	void Draw();
	void virtual ReDraw(Direction);
	bool virtual CanMove(Matrix, Direction);
	Point GetPoint();
	bool operator == (Actor);
	void gotoXY(Point);
	void gotoXY(int, int);
	~Actor();
};

